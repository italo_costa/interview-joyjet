/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author italo
 */
public class Data {
    
    private List<Article> articles;
    private List<Cart> carts;
    private List<DeliveryFee> delivery_fees;
    private List<Discounts> discounts;

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    public List<DeliveryFee> getDelivery_fees() {
        return delivery_fees;
    }

    public void setDelivery_fees(List<DeliveryFee> delivery_fees) {
        this.delivery_fees = delivery_fees;
    }

    public List<Discounts> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Discounts> discounts) {
        this.discounts = discounts;
    }

    @Override
    public String toString() {
        return "Data{" + "articles=" + articles + ", carts=" + carts + ", delivery_fees=" + delivery_fees + ", discounts=" + discounts + '}';
    }
    
    
}
