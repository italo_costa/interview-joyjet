/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entrevistajoyjet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import modelo.Article;
import modelo.Cart;
import modelo.Data;
import modelo.Item;
import modelo.Output;

/**
 *
 * @author italo
 */
public class Level1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Gson gson = new Gson();
        try {
            Data data = gson.fromJson(readFile(), Data.class);
            calculaTotalCarts(data);
            
            Output output = new Output(data.getCarts());
            gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String json = gson.toJson(output);
            writeFile(json);
        } catch (FileNotFoundException ex) {
            System.err.println("Não foi possível ler o arquivo JSON.\n"+ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Não foi possível escrever o arquivo JSON.\n"+ex.getMessage());
        }
        
    }
    
    private static void calculaTotalCarts(Data data){
        for(Cart cart : data.getCarts()){
            Long total = 0L;
            for(Item item : cart.getItems()){
                int index = data.getArticles().indexOf(new Article(item.getArticle_id()));
                Long price = data.getArticles().get(index).getPrice();
                total += item.getQuantity() * price;
            }
            cart.setTotal(total);
        }
    }
    
    private static FileReader readFile() throws FileNotFoundException{
        return new FileReader("data.json");
    }
    
    private static void writeFile(String json) throws IOException{
        FileWriter writeFile = new FileWriter("output.json");
        writeFile.write(json);
	writeFile.close();
    }
    
}
