/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import com.google.gson.annotations.Expose;
import java.util.List;

/**
 *
 * @author italo
 */
public class Output {

    public Output(List<Cart> carts) {
        this.carts = carts;
    }
    
    @Expose
    private List<Cart> carts;

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    @Override
    public String toString() {
        return "Output{" + "carts=" + carts + '}';
    }
    
    
    
}
