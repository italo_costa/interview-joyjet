/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author italo
 */
public class DeliveryFee {
    
    private EligibleTransactionVolume eligible_transaction_volume;
    private Long price;

    public EligibleTransactionVolume getEligible_transaction_volume() {
        return eligible_transaction_volume;
    }

    public void setEligible_transaction_volume(EligibleTransactionVolume eligible_transaction_volume) {
        this.eligible_transaction_volume = eligible_transaction_volume;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
    
    
}